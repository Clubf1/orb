/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

_orb_directory_update_callbacks = [];

/* Directory updates
 */
function orb_directory_upon_update(callback) {
	_orb_directory_update_callbacks.push(callback);
}

function orb_directory_notify_update(directory) {
	orb_directory_exists(directory, function(exists) {
		if (exists == false) {
			directory = orb_file_dirname(directory);
		}

		_orb_directory_update_callbacks.forEach(function(callback) {
			callback(directory);
		});
	});
}

/* Directory operations
 */
function orb_directory_list(path, callback_done, callback_fail = undefined) {
    $.ajax({
        url: '/orb/dir/list/' + path
    }).done(function(data) {
		var items = [];
        $(data).find('item').each(function() {
			var item = {
				name: $(this).find('name').text(),
				type: $(this).find('type').text(),
				link: $(this).find('link').text() == 'yes',
				target: $(this).find('target').text(),
				size: $(this).find('size').text(),
				create: $(this).find('create').text(),
				create_timestamp: $(this).find('create').attr('timestamp'),
				access: $(this).find('access').text(),
				access_timestamp: $(this).find('access').attr('timestamp')
			}
			items.push(item);
		});

		callback_done(items);
	}).fail(function(result) {
		if (callback_fail != undefined) {
			callback_fail(result.status);
		}
	});
}

function orb_directory_exists(directory, callback_done, callback_fail = undefined) {
	$.ajax({
		url: '/orb/dir/exists/' + directory
	}).done(function(data) {
		callback_done($(data).find('exists').text() == 'yes');
	}).fail(function(result) {
		if (callback_fail != undefined) {
			callback_fail(result.status);
		}
	});
}

function orb_directory_create(directory, callback_done = undefined, callback_fail = undefined) {
	$.post('/orb/dir/make', {
		directory: directory
	}).done(function() {
		directory = orb_file_dirname(directory);
		orb_directory_notify_update(directory);

		if (callback_done != undefined) {
			callback_done();
		}
	}).fail(function(result) {
		if (callback_fail != undefined) {
			callback_fail(result.status);
		}
	});
}

function orb_directory_remove(directory, callback_done = undefined, callback_fail = undefined) {
    directory = orb_file_prepare(directory);

    $.post('/orb/dir/remove', {
		directory: directory
    }).done(function(data) {
        directory = orb_file_dirname(directory);
        orb_directory_notify_update(directory);

        if (callback_done != undefined) {
            callback_done();
        }
    }).fail(function(result) {
        if (callback_fail != undefined) {
            callback_fail(result.status);
        }
    });
}
