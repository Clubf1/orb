/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

function diskview_menu_click(diskview_window, item) {
	diskview_window = diskview_window.find('div.diskview');

	switch (item) {
		case 'Home directory':
			diskview_start(diskview_window, 'Home directory', 'homedir');
			break;
		case 'Applications':
			diskview_start(diskview_window, 'Applications', 'apps');
			break;
		case 'Exit':
			diskview_window.close();
			break;
		case 'About':
			orb_alert('Disk allocation viewer\nCopyright (c) by Hugo Leisink', 'About');
			break;
	}
}

function diskview_calc_totals(data) {
	var total = 0;
	
	$(data).children('directory').each(function() {
		var size = parseInt($(this).attr('size'));
		size += diskview_calc_totals($(this));
		$(this).attr('total', size);

		total += size;
	});

	return total;
}

function diskview_get_tree(data, total_size) {
	var tree = '';

	$(data).children('directory').each(function() {
		var size = parseInt($(this).attr('size'));
		var total = parseInt($(this).attr('total'));
		var perc = ((100 * total) / total_size).toFixed(1);

		tree += '<div class="directory">' +
			'<div class="perc-container">' +
			'<div class="perc" style="width:' + perc + '%"></div>' +
			'</div>' +
			'<div class="line" title="' + orb_file_nice_size(size, true) + '">' +
			'<span class="name">' + $(this).attr('name') + '</span>' +
			'<span class="perc">' + perc + '%</span>' +
			'<span class="size">' + orb_file_nice_size(total) + '</span>' +
			'</div>' +
			diskview_get_tree($(this), total) +
			'</div>';
	});

	return tree;
}

function diskview_start(diskview_window, title, section) {
	diskview_window.empty();

	$.ajax({
		url: '/diskview/' + section,
	}).done(function(data) {
		var directories = $(data).children('output').first();

		var total_size = diskview_calc_totals(directories);
		var tree = diskview_get_tree(directories, total_size);
		diskview_window.append(tree);

		diskview_window.children('div.directory').show();
		diskview_window.children('div.directory').children('div.directory').show();
		diskview_window.children('div.directory').children('div.line').find('span.name').text(title);

		diskview_window.find('div.directory').each(function() {
			if ($(this).children('div.directory').length > 0) {
				var type = $(this).children('div.directory').first().is(':visible') ? 'down' : 'right';
				$(this).children('div.line').find('span.name').before('<span class="fa fa-chevron-' + type + '"></span>');
			}
		});

		diskview_window.find('div.directory span').click(function(event) {
			var dir = $(this).parent().parent();
			dir.children('div.directory').toggle();
			var type = dir.children('div.directory').first().is(':visible') ? 'down' : 'right';
			dir.children('div.line').find('span.fa').removeClass('fa-chevron-down');
			dir.children('div.line').find('span.fa').removeClass('fa-chevron-right');
			dir.children('div.line').find('span.fa').addClass('fa-chevron-' + type);
			event.stopPropagation();
		});

		if (section == 'homedir') {
			$.ajax({
				url: '/diskview/capacity',
			}).done(function(data) {
				capacity = $(data).find('capacity').text();
				diskview_window.append('<div class="capacity">Maximum capacity: ' + capacity + '</div>');
			});
		}
	});
}

function diskview_open(filename = undefined) {
	var window_content =
		'<div class="diskview"></div>';

	var diskview_window = $(window_content).orb_window({
		header:'DiskView',
		icon: '/apps/diskview/diskview.png',
		width: 760,
		height: 430,
        menu: {
            'File': [ 'Home directory', 'Applications', '-', 'Exit' ],
            'Help': [ 'About' ]
        },
        menuCallback: diskview_menu_click
	});

	diskview_window.open();

	diskview_start(diskview_window, 'Home directory', 'homedir');
}

$(document).ready(function() {
	orb_startmenu_add('DiskView', '/apps/diskview/diskview.png', diskview_open);
});
