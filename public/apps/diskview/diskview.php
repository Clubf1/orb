<?php
	if (defined("ORB_VERSION") == false) exit;

	class diskview extends orb_backend {
		private function directory_size($path, $name) {
			$path = $path."/".$name;

			if (($dp = opendir($path)) == false) {
				return false;
			}

			$dirs = array();
			$size = 0;
			while (($file = readdir($dp)) != false) {
				if (substr($file, 0, 1) == ".") {
					continue;
				}

				if (is_dir($path."/".$file)) {
					array_push($dirs, $file);
				} else if (($filesize = filesize($path."/".$file)) !== false) {
					$size += $filesize;
				}
			}

			$this->view->open_tag("directory", array("name" => $name, "size" => $size));

			sort($dirs);
			foreach ($dirs as $dir) {
				$this->directory_size($path, $dir);
			}

			$this->view->close_tag();

			closedir($dp);
		}

		public function get_homedir() {
			$this->directory_size($this->home_directory, ".");
		}

		public function get_apps() {
			$this->directory_size("apps", ".");
		}

		public function get_capacity() {
			$users = file(PASSWORD_FILE);

			$capacity = "unlimited";
			foreach ($users as $user) {
				$parts = explode(":", trim($user));

				if ($parts[0] != $this->username) {
					continue;
				} else if (count($parts) < 3) {
					continue;
				} else if ($parts[2] == "") {
					continue;
				}

				$capacity = (int)$parts[2]." MB";
			}

			$this->view->add_tag("capacity", $capacity);
		}
	}
?>
