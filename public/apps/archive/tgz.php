<?php
	class tgz extends archiver_library implements archiver_interface {
		private $archive = null;
		private $tgz = null;

		public function open($archive) {
			try {
				$tgz = new PharData($archive);
			} catch (Exception $e) {
				return false;
			}

			$this->archive = $archive;
			$this->tgz = $tgz;

			return true;
		}

		private function list_dir(&$list, $dir, $path = "") {
			foreach ($dir as $file) {
				$name = $path.$file->getFilename();
				if ($file->isDir()) {
					$this->list_add($list, $name."/", 0);
					$this->list_dir($list, new PharData($file->getPathname()), $name."/");
				} else {
					$this->list_add($list, $name, $file->getSize());
				}
			}
		}

		public function list() {
			if ($this->tgz === null) {
				return false;
			}

			$list = array();
			$this->list_dir($list, $this->tgz);
			return $list;
		}

		public function extract($filename, $directory) {
			try {
				$this->tgz->extractTo($directory, $filename, true);
			} catch (Exception $e) {
				return false;
			}

			return $directory."/".$filename;
		}
	}
?>
