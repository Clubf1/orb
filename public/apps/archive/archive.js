/* Orb archive application
 *
 * Let every function in this file start with the application name
 * to avoid conflicts with other applications,
 *
 * Always use archive_window to interact with your application to
 * avoid issues with multiple instances of your application.
 */

const archive_extensions = [ 'zip', 'tgz' ];

function archive_valid_extension(filename) {
	var extension = orb_file_extension(filename);

	if (extension == false) {
		return false;
	}

	return archive_extensions.includes(extension.toLowerCase());
}

function archive_list(container, list, directory = null) {
	if (directory == null) {
		directory = '';
	}

	var new_container = $('<div class="directory"><span class="name">' + list.attr('name') + '</span></div>');
	container.append(new_container);
	container = new_container;

	list.children('directory').each(function() {
		archive_list(container, $(this), directory + $(this).attr('name') + '/');
	});

	list.children('file').each(function() {
		var extension = orb_file_extension($(this).text());
		var icon = orb_get_file_icon(extension);
		var size = orb_file_nice_size($(this).attr('size'));
		var file = '<div class="file" path="' + directory + '">' +
			'<img src="' + icon + '" class="icon" draggable="false" />' +
			'<span class="name">' + $(this).text() + '</span>' +
			'<span class="size">' + size + '</span>' +
			'</div>';
		container.append(file);
	});
}

function archive_open_archive(archive_window, archive) {
	$.ajax({
		url: '/archive/list/' + url_encode(archive)
	}).done(function(data) {
		archive_window.empty();
		archive_list(archive_window, $(data).find('output').children('directory'));

		archive_window.children('div').show();
		archive_window.children('div').children('div').show();

		archive_window.find('div.directory').each(function() {
			if ($(this).children('div').length > 0) {
				var type = $(this).children('div').first().is(':visible') ? 'down' : 'right';
				$(this).prepend('<span class="fa fa-chevron-' + type + '"></span>');
			}
		});

		archive_window.find('div.file').click(function() {
			archive_window.find('div.selected').removeClass('selected');
			$(this).addClass('selected');
		});

		archive_window.find('div.file span').click(function() {
			$(this).parent().trigger('click');
		});

		archive_window.find('div.file').dblclick(function() {
			var filename = $(this).attr('path') + $(this).find('span.name').text();
			archive_preview(archive_window, archive, filename);
		});

		archive_window.find('div.directory span').click(function(event) {	
			var dir = $(this).parent();
			dir.children('div').toggle();

            var type = dir.children('div').first().is(':visible') ? 'down' : 'right';
            dir.children('span.fa').removeClass('fa-chevron-down');
           	dir.children('span.fa').removeClass('fa-chevron-right');
            dir.children('span.fa').addClass('fa-chevron-' + type);
			return false;
		});
	}).fail(function() {
		orb_alert('Error opening archive.');
	});
}

function archive_preview(archive_window, archive, filename) {
	var extension = orb_file_extension(filename);
	var handler = orb_get_file_handler(extension);
	var archive_work_dir = 'Temporary';

	$.post('/archive/extract', {
		archive: archive,
		filename: filename,
		directory: archive_work_dir
	}).done(function(data) {
		var filename = $(data).find('filename').text();

		orb_directory_notify_update(archive_work_dir);
		if (handler != undefined) {
			handler(filename);
		} else {
			window.open(orb_download_url(file), '_blank').focus();
		}
	}).fail(function(data) {
		
	});
}

function archive_menu_click(archive_window, item) {
	archive_window = archive_window.find('div.archive');

	switch (item) {
		case 'Open':
			orb_file_dialog('Open', function(filename) {
				if (archive_valid_extension(filename) == false) {
					orb_alert('Invalid file type.');
				} else {
					archive_open_archive(archive_window, filename);
				}
			});
			break;
		case 'Exit':
			archive_window.close();
			break;
		case 'About':
			var extensions = archive_extensions.join(', ');
			orb_alert('Archive browser\nCopyright (c) by Hugo Leisink\nSupported file extensions: ' + extensions, 'About');
			break;
	}
}

function archive_open(filename = undefined) {
	var window_content =
		'<div class="archive">' +
		'</div>';

	var archive_window = $(window_content).orb_window({
		header:'Archive',
		icon: '/apps/archive/archive.png',
		width: 760,
		height: 430,
		menu: {
			'File': [ 'Open', 'Exit' ],
			'Help': [ 'About' ]
		},
		menuCallback: archive_menu_click
	});

	archive_window.open();

	if (filename != undefined) {
		archive_open_archive(archive_window, filename);
	}
}

$(document).ready(function() {
	var icon = '/apps/archive/archive.png';
	orb_startmenu_add('Archive', icon, archive_open);

	archive_extensions.forEach(function(ext) {
		orb_upon_file_open(ext, archive_open, icon);
	});
});
