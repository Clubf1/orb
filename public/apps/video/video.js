/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

var video_extensions = [ 'avi', 'mpg', 'mp4', 'mov' ];

function video_valid_extension(filename) {
	var extension = orb_file_extension(filename);

	if (extension == false) {
		return false;
	}

	return video_extensions.includes(extension.toLowerCase());
}

function video_play(video_window, filename) {
	video_window.find('video').remove();
	video_window.append('<video controls><source src="' + orb_download_url(filename) + '" /></video>');
	video_window.find('video')[0].play();
}

function video_open(filename = undefined) {
	var window_content =
		'<div class="video">' +
		'<div class="name"></div>' +
		'<video controls></video>' +
		'</div>';

	var video_window = $(window_content).orb_window({
		header:'Video',
		icon: '/apps/video/video.png',
		width: 500,
		height: 270,
		menu: {
			'File': [ 'Open', 'Exit' ],
			'Help': [ 'About' ]
		},
		menuCallback: video_menu_click
	});

	video_window.open();

	if (filename != undefined) {
		video_play(video_window, filename);
	}
}

function video_menu_click(video_window, item) {
	video_window = video_window.find('div.video');

	switch (item) {
		case 'Open':
			orb_file_dialog('Open', function(filename) {
				if (video_valid_extension(filename) == false) {
					orb_alert('Invalid file type.');
				} else {
					video_play(video_window, filename);
				}
			});
			break;
		case 'Exit':
			video_window.close();
			break;
		case 'About':
			orb_alert('Video player\nCopyright (c) by Hugo Leisink', 'About');
			break;
	}
}

$(document).ready(function() {
	var icon = '/apps/video/video.png';
	orb_startmenu_add('Video', icon, video_open);
	video_extensions.forEach(function(extension) {
		orb_upon_file_open(extension, video_open, icon);
	});
});
