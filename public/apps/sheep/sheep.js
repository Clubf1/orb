/* Orb sheep application
 *
 * Let every function in this file start with the application name
 * to avoid conflicts with other applications,
 *
 * Always use sheep_window to interact with your application to
 * avoid issues with multiple instances of your application.
 */

function sheep_open(filename = undefined) {
	orb_load_javascript('/apps/sheep/esheep.js');

	var sheep = new eSheep();
	sheep.Start();
}

$(document).ready(function() {
	orb_startmenu_add('Sheep', '/apps/sheep/sheep.png', sheep_open);
});
