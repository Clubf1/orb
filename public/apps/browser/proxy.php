<?php
	/* Proxy library
	 *
	 * Written by Hugo Leisink <hugo@leisink.net>
	 */

	define("CONNECTION_ERROR", -1);
	define("LOOPBACK",		   -2);

	class proxy extends HTTP {
		const COOKIE_LABEL = "proxy";

		private $hostname = null;
		private $base_url = null;
		private $base_path = null;
		private $proxy_url = null;
		private $proxy_hostname = null;
		private $headers_to_server = array("Accept", "Accept-Charset",
			"Accept-Language", "Referer", "User-Agent", "Authorization");
		private $headers_to_client = array("Accept-Ranges", "Cache-Control",
			"Content-Type", "Content-Range", "DNT", "ETag", "Expires",
			"Last-Modified", "Location", "Pragma", "Refresh",
			"WWW-Authenticate");

		/* Constructor
		 *
		 * INPUT:  string hostname[, int port]
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function __construct($hostname, $port = null) {
			$this->hostname = $hostname;

			list($this->proxy_url) = explode("?", $_SERVER["REQUEST_URI"], 2);

			parent::__construct($hostname, $port);
		}

		/* Forward HTTP header from browser to server
		 *
		 * INPUT:  string header
		 * OUTPUT: -
		 * ERROR:  -
		 */
		private function header_to_server($header) {
			$envkey = "HTTP_".str_replace("-", "_", strtoupper($header));

			if (isset($_SERVER[$envkey]) == false) {
				return;
			}

			$value = $_SERVER[$envkey];
			if ($header == "Referer") {
				$value = str_replace("/".$this->hostname."/", "/", $value);
				$value = str_replace("://".$_SERVER["SERVER_NAME"]."/", "://".$this->hostname."/", $value);
			}

			$this->add_header($header, $value);
		}

		/* Rewrite URL
		 *
		 * INPUT:  string url
		 * OUTPUT: string url
		 * ERROR:  -
		 */
		private function rewrite_url($url) {
			$url = trim($url, " \"'\n");

			if (substr($url, 0, 5) == "data:") {
				return $url;
			}

			$new_url = "https://".$_SERVER["SERVER_NAME"].$this->proxy_url."?url=";

			if ((substr($url, 0, 2) == "//") && (strlen($url) > 2)) {
				$new_url .= "http";
				if ($this->protocol == "tls") {
					$new_url .= "s";
				}
				$new_url .= ":".$url;
			} else if (substr($url, 0, 7) == "http://") {
				$new_url .= $url;
			} else if (substr($url, 0, 8) == "https://") {
				$new_url .= $url;
			} else if (substr($url, 0, 1) == "/") {
				$new_url .= $this->base_url.$url;
			} else if (rtrim($url, "/") == "..") {
				$new_url .= $this->base_url.dirname($this->base_path);
			} else {
				$new_url .= $this->base_url.$this->base_path.$url;
			}

			return $new_url;
		}

		/* Fix property value
		 *
		 * INPUT:  string data, string delimeter begin, string delimeter end
		 * OUTPUT: -
		 * ERROR:  -
		 */
		private function rewrite_to_proxy($data, $delim_begin, $delim_end) {
			$offset = 0;

			while (($begin = strpos($data, $delim_begin, $offset)) !== false) {
				$begin += strlen($delim_begin);
				if (($end = strpos($data, $delim_end, $begin)) === false) {
					$offset = $begin;
					continue;
				}

				$first = substr($data, 0, $begin);
				$url = substr($data, $begin, $end - $begin);
				$last = substr($data, $end);

				$data = $first.$this->rewrite_url($url).$last;

				$offset = $begin + strlen($url) + 1;
			}

			return $data;
		}

		/* Update the cookie path with the hostname
		 */
		private function cookie_path(&$value) {
			$path = "/";

			$parts = explode(";", $value);

			foreach ($parts as $i => $part) {
				$c_parts = explode("=", trim($part));
				$c_key = array_shift($c_parts);
				$c_value = array_shift($c_parts);

				if ($c_value === null) {
					continue;
				}

				$c_key = trim($c_key);

				if (strtolower($c_key) == "domain") {
					$parts[$i] = $c_key."=".$_SERVER["SERVER_NAME"];
				} else if (strtolower($c_key) == "path") {
					$path = rtrim($c_value, "/")."/";
					unset($parts[$i]);
				}
			}

			$value = implode(";", $parts);

			return $path;
		}

		/* Remove .. from path.
		 *
		 * INPUT:  string path
		 * OUTPUT: string path
		 * ERROR:  -
		 */
		private function fix_path($path) {
			$fixed = array();

			$parts = explode("/", $path);
			foreach ($parts as $part) {
				if ($part == "..") {
					array_pop($fixed);
				} else {
					array_push($fixed, $part);
				}
			}

			return implode("/", $fixed);
		}

		/* Forward request to remote webserver
		 *
		 * INPUT:  string path
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function forward_request($path) {
			/* Proxy self?
			 */
			if ($this->hostname == $_SERVER["SERVER_NAME"]) {
				return LOOPBACK;
			}

			$path = $this->fix_path($path);

			/* Base url and path
			 */
			$this->base_url = "http";
			if ($this->protocol == "tls") {
				$this->base_url .= "s";
			}
			$this->base_url .= "://".$this->hostname;

			if (($pos = strrpos($path, "/")) !== false) {
				$this->base_path = substr($path, 0, $pos + 1);
			} else {
				$this->base_path = "/";
			}

			/* Forward headers to server
			 */
			foreach ($this->headers_to_server as $header) {
				$this->header_to_server($header);
			}
			#$this->add_header("X-Forwarded-For", $_SERVER["REMOTE_ADDR"]);

			/* POST data
			 */
			$parts = array();
			foreach ($_POST as $key => $value) {
				array_push($parts, urlencode($key)."=".urlencode($value));
			}
			$post = implode("&", $parts);

			/* Cookies from client
			 */
			foreach ($_COOKIE as $name => $c_value) {
				$parts = explode("|", $name, 4);
				$c_proxy = array_shift($parts);
				$c_hostname = array_shift($parts);
				$c_key = array_shift($parts);
				$c_path = array_shift($parts);

				if ($c_proxy != self::COOKIE_LABEL) {
					continue;
				}

				if (substr($path, 0, strlen($c_path)) != $c_path) {
					continue;
				}

				if ($c_hostname != str_replace(".", "_", $this->hostname)) {
					continue;
				}

				$this->add_cookie($c_key, $c_value);
			}

			/* Send request to server
			 */
			switch ($_SERVER["REQUEST_METHOD"]) {
				case "GET":
					$result = $this->GET($path);
					break;
				case "POST":
					$result = $this->POST($path, $post);
					break;
				default:
					return 405;
			}

			/* Abort on error
			 */
			if ($result === false) {
				return CONNECTION_ERROR;
			} else if ($result["status"] >= 500) {
				return $result["status"];
			}

			/* Fix headers
			 */
			if (isset($result["headers"]["location"])) {
				$result["headers"]["location"] = $this->rewrite_url($result["headers"]["location"]);
			}

			/* Fix body
			 */
			if (substr($result["headers"]["content-type"], 0, 9) == "text/html") {
				/* HTML
				 */
				foreach (array("action", "href", "src", "srcset") as $property) {
					$result["body"] = $this->rewrite_to_proxy($result["body"], $property.'="', '"');
				}
				$result["body"] = $this->rewrite_to_proxy($result["body"], "url(", ")");
				$result["body"] = $this->rewrite_to_proxy($result["body"], "='", "'");
				$result["body"] = $this->rewrite_to_proxy($result["body"], "( '", "'");
				$result["body"] = str_replace(" target=\"_blank\"", "", $result["body"]);
			} else if (substr($result["headers"]["content-type"], 0, 8) == "text/css") {
				/* CSS
				 */
				$result["body"] = $this->rewrite_to_proxy($result["body"], "url(", ")");
				/* Javascript
			} else if ((substr($result["headers"]["content-type"], 0, 15) == "text/javascript") ||
			           (substr($result["headers"]["content-type"], 0, 22) == "application/javascript") ||
			           (substr($result["headers"]["content-type"], 0, 24) == "application/x-javascript")) {
				$result["body"] = $this->rewrite_to_proxy($result["body"], "='", "'");
				$result["body"] = $this->rewrite_to_proxy($result["body"], '+"', '"');
				*/
			}

			/* Add base tag
			 */
			$base_url = "https://".$_SERVER["SERVER_NAME"].$this->proxy_url."?url=".$this->base_url;
			$result["body"] = str_replace("<head>", "<head>\n<base href=\"".$base_url."\" />", $result["body"]);

			/* Send result to browser
			 */
			if ($result["status"] != 200) {
				header("Status: ".$result["status"]);
			}

			foreach ($this->headers_to_client as $key) {
				if (($value = $result["headers"][strtolower($key)] ?? null) != null) {
					header($key.": ".$value);
				}
			}

			foreach ($result["headers"] as $key => $value) {
				if (strtolower(substr($key, 2)) == "x-") {
					header($key.": ".$value);
				}
			}

			foreach ($result["cookies"] as $key => $value) {
				$path = $this->cookie_path($value);
				$key = sprintf("%s|%s|%s|%s", self::COOKIE_LABEL, $this->hostname, $key, $path);
				header(sprintf("Set-Cookie: %s=%s", $key, $value), false);
			}

			print $result["body"];

			if ($result["sock"] !== null) {
				while (($line = fgets($result["sock"])) !== false) {
					print $line;
				}

				fclose($result["sock"]);
				$result["sock"] = null;
			}

			return 0;
		}
	}

	class proxys extends proxy {
		protected $default_port = 443;
		protected $protocol = "tls";
	}
?>
