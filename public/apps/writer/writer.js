/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

function writer_focus(writer_window) {
	var editor = writer_window.data('ckeditor');
	editor.focus();
}

function writer_dont_discard(writer_window) {
	if (writer_window.data('changed')) {
		if (confirm('File has been changed. Discard?') == false) {
			return true;
		}
	}

	return false;
}

function writer_valid_extension(filename) {
	var extension = orb_file_extension(filename);

	if (extension == false) {
		return false;
	}

	var extensions = [ 'owd', 'htm', 'html' ];

	return extensions.includes(extension.toLowerCase());
}

function writer_save_file(writer_window, filename) {
	var ckeditor = writer_window.data('ckeditor');
	var content = ckeditor.getData();

	orb_file_save(filename, content, false, function() {
		writer_window.data('changed', false);
		writer_window.data('filename', filename);
	}, function() {
		orb_alert('Error while saving file.', 'Error');
	});
}

function writer_insert_image(button) {
	for (var i = 0; i < 9; i++) {
		button = $(button).parent();
	}
	var ckeditor = $(button).find('div.writer').data('ckeditor');

	orb_file_dialog('Open', function(filename) {
		var extension = orb_file_extension(filename);
		if (extension == 'jpg') {
			extension = 'jpeg';
		}

		var extensions = [ 'gif', 'jpeg', 'png' ];

		if (extensions.includes(extension) == false) {
			orb_alert('Invalid file type.');
			return;
		}

		orb_file_open(filename, function(image) {
			image = 'data:image/' + extension + ';base64, ' + btoa(image);
			ckeditor.execute('imageInsert', { source: image } );
		});
	});
}

function writer_menu_click(writer_window, item) {
	writer_window = writer_window.find('div.writer');
	var ckeditor = writer_window.data('ckeditor');

	switch (item) {
		case 'New':
			if (writer_dont_discard(writer_window)) {
				return false;
			}

			ckeditor.setData('');
			writer_window.data('changed', false);
			writer_window.data('filename', null);
			writer_focus(writer_window);
			break;
		case 'Open':
			if (writer_dont_discard(writer_window)) {
				return false;
			}

			orb_file_dialog('Open', function(filename) {
				if (writer_valid_extension(filename) == false) {
					orb_alert('Invalid file type.');
				} else {
					orb_file_open(filename, function(content) {
						ckeditor.setData(content);
						writer_window.data('changed', false);
						writer_window.data('filename', filename);
						writer_focus(writer_window);
					}, function() {
						orb_alert('File not found.', 'Error');
					});
				}
			}, 'Documents');
			break;
		case 'Save':
			var filename = writer_window.data('filename');
			if (filename != undefined) {
				writer_save_file(writer_window, filename);
				break;
			}
		case 'Save as':
			var filename = writer_window.data('filename');
			if (filename == null) {
				var start_dir = 'Documents';
				var start_file = undefined;
			} else {
				var start_dir = orb_file_dirname(filename);
				var start_file = orb_file_filename(filename);
			}

			orb_file_dialog('Save', function(filename) {
				if (writer_valid_extension(filename) == false) {
					filename += '.owd';
				}

				if (filename != writer_window.data('filename')) {
					orb_file_exists(filename, function(exists) {
						if (exists) {
							if (confirm('File already exists. Overwrite?') == false) {
								return;
							}
						}
						writer_save_file(writer_window, filename);
					}, function() {
						orb_alert('Error while saving file.', 'Error');
					});
				} else {
					writer_save_file(writer_window, filename);
				}
			}, start_dir, start_file);
			break;
		case 'Generate PDF':
			var filename = writer_window.data('filename');
			if (filename == undefined) {
				filename = 'Temporary/document.pdf';
			} else {
				filename = orb_file_filename(filename);
				var pos = filename.lastIndexOf('.');
				if (pos != undefined) {
					filename = filename.substr(0, pos);
				}
				filename = 'Temporary/' + filename + '.pdf';
			}

			var ckeditor = writer_window.data('ckeditor');
			var content = ckeditor.getData();

			$.post('/writer/pdf', {
				filename: filename,
				content: content
			}).done(function(data) {
				orb_desktop_refresh();
				pdf_open(filename);
			}).fail(function() {
				orb_alert('Error generating PDF.');
			});
			break;
		case 'Exit': writer_window.close();
			break;
		case 'About':
			orb_alert('Writer\nCopyright (c) by Hugo Leisink\n\nCKEditor\nCopyright (c) by CKSource\n<a href="https://ckeditor.com/" target="_blank">https://ckeditor.com/</a>', 'About');
			break;
	}
}

function writer_open_icon(icon) {
	var filename = orb_icon_to_filename(icon);

	if (filename == undefined) {
		return;
	}

	writer_open(filename);
}

function writer_open(filename = undefined) {
	var window_content =
		'<div class="writer"><div class="editor"></div><div class="editor-counters"></div></div>';

	var writer_window = $(window_content).orb_window({
		header:'Writer',
		icon: '/apps/writer/writer.png',
		width: 800,
		minwidth: 645,
		height: 500,
		menu: {
			'File': [ 'New', 'Open', 'Save', 'Save as', '-', 'Generate PDF', '-', 'Exit' ],
			'Help': [ 'About' ]
		},
		menuCallback: writer_menu_click
	});

	writer_window.on('dragover', function(event) {
		event.stopPropagation();
	});

	writer_window.data('changed', false);
	writer_window.data('filename', null);

	orb_load_javascript('/apps/writer/ckeditor.js');

	ClassicEditor.create(writer_window.find('div.editor')[0], {
		toolbar: {
			items:["heading","|","fontFamily","fontSize","fontColor","|","bold","italic","underline","strikethrough","subscript","superscript","|","bulletedList","numberedList","todoList","-","alignment","outdent","indent","|","findAndReplace","undo","redo","|","link","specialCharacters","mediaEmbed","blockQuote","insertTable","code","codeBlock"], //"|","sourceEditing"],
			shouldNotGroupWhenFull: true
		}
	}).then(editor => {
		writer_window.find('div.ck-editor').css('height', 'calc(100% - 20px)');
		writer_window.find('div.ck-editor__main').css('height', 'calc(100% - 76px)');

		const wordCountPlugin = editor.plugins.get('WordCount');
		const wordCountWrapper = writer_window.find('div.editor-counters')[0];
		wordCountWrapper.appendChild(wordCountPlugin.wordCountContainer);

		writer_window.data('ckeditor', editor);

		editor.model.document.on('change', function() {
			writer_window.data('changed', true);
		});

		var icon = '<svg class="ck ck-icon ck-button__icon" viewBox="0 0 20 20"><path d="M6.91 10.54c.26-.23.64-.21.88.03l3.36 3.14 2.23-2.06a.64.64 0 0 1 .87 0l2.52 2.97V4.5H3.2v10.12l3.71-4.08zm10.27-7.51c.6 0 1.09.47 1.09 1.05v11.84c0 .59-.49 1.06-1.09 1.06H2.79c-.6 0-1.09-.47-1.09-1.06V4.08c0-.58.49-1.05 1.1-1.05h14.38zm-5.22 5.56a1.96 1.96 0 1 1 3.4-1.96 1.96 1.96 0 0 1-3.4 1.96z"></path></svg>';
		var tooltip = '<span class="ck ck-tooltip ck-tooltip_s"><span class="ck ck-tooltip__text">Insert image</span></span>';
		var button = '<button class="ck ck-button ck-off" type="button" tabindex="-1" aria-labelledby="ck-editor__aria-label_e1ef8259b9c8dfde5e02286d9851b0ae5" onClick="javascript:writer_insert_image(this)">' + icon + tooltip + '</button>';
		writer_window.find('div.ck-toolbar__items div:nth-of-type(9)').after(button);

		if (filename != undefined) {
			if (writer_valid_extension(filename) == false) {
				writer_window.close();
				orb_alert('Invalid file type.');
			} else {
				orb_file_open(filename, function(content) {
					editor.setData(content);
					writer_window.data('changed', false);
					writer_window.data('filename', filename);
					writer_window.open();
					writer_focus(writer_window);
				}, function() {
					writer_window.close();
					orb_alert('File not found.');
				});
			}
		} else {
			writer_window.open();
			writer_focus(writer_window);
		}
	}).catch(error => {
		console.error(error);
	});
}

$(document).ready(function() {
	var icon = '/apps/writer/writer.png';
	orb_startmenu_add('Writer', icon, writer_open);
	orb_upon_file_open('owd', writer_open, icon);

	orb_contextmenu_extra_item('html', 'Edit with Writer', 'i-cursor', writer_open_icon);
});
