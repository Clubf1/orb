/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

function notepad_focus(notepad_window) {
	notepad_window.find('textarea').focus();
}

function notepad_dont_discard(notepad_window) {
	if (notepad_window.data('changed')) {
		if (confirm('File has been changed. Discard?') == false) {
			return true;
		}
	}

	return false;
}

function notepad_valid_extension(filename) {
	var extension = orb_file_extension(filename);

	if (extension == false) {
		return true;
	}

	var extensions = [ 'txt', 'ots', 'log', 'html', ORB_NO_EXTENSION ];

	return extensions.includes(extension.toLowerCase());
}

function notepad_save_file(notepad_window, filename) {
	var content = notepad_window.find('textarea').val();

	orb_file_save(filename, content, false, function() {
		notepad_window.data('changed', false);
		notepad_window.data('filename', filename);
	}, function() {
		orb_alert('Error while saving file.', 'Error');
	});
}

function notepad_set_cursor(notepad_window, position, select = 0) {
	var elem = notepad_window.find('textarea')[0];

	elem.focus();
	elem.setSelectionRange(position, position + select);
}

function notepad_tab_pressed(notepad_window, event) {
	var textarea = notepad_window.find('textarea');

	if (event.which != 9) {
		return;
	}

	event.preventDefault();
	var start = textarea[0].selectionStart;
	var end = textarea[0].selectionEnd;
	var text = textarea.val();
	var selText = text.substring(start, end);
	textarea.val(text.substring(0, start) + '\t' +
		selText.replace(/\n/g, '\n\t') +
		text.substring(end));
	textarea[0].selectionStart = textarea[0].selectionEnd = start + 1;

	notepad_window.data('changed', true);
}

function notepad_find(notepad_window) {
	var find_content =
		'<div class="notepad_find_replace">' +
		'<div class="form-group">' +
		'<input type="text" placeholder="Find" class="form-control find" />' +
		'</div>' +
		'<div class="btn-group">' +
		'<input type="button" value="Find" class="btn btn-default action" />' +
		'<input type="button" value="Cancel" class="btn btn-default cancel" />' +
		'</div>' +
		'</div>';

	var notepad_find_window = $(find_content).orb_window({
		header: 'Find',
		width: 500,
		height: 100,
		icon: '/apps/notepad/notepad.png',
		dialog: true
	});

	notepad_find_window.find('input.action').click(function() {
		var find = notepad_find_window.find('input.find').val();

		if (find != '') {
			var text = notepad_window.find('textarea').val();
			if ((position = text.indexOf(find)) > -1) {
				notepad_window.data('find_str', find);
				notepad_window.data('find_pos', position);
				notepad_set_cursor(notepad_window, position, find.length);
			} else {
				orb_alert('Text not found.');
			}

			notepad_find_window.close();
		}
	});

	notepad_find_window.find('input.cancel').click(function() {
		notepad_find_window.close();
	});

	notepad_find_window.open();
	notepad_find_window.find('input.find').focus();
}

function notepad_find_next(notepad_window) {
	var find = notepad_window.data('find_str');
	var position = notepad_window.data('find_pos');

	if ((find != undefined) && (position != undefined)) {
		var text = notepad_window.find('textarea').val();
		if ((position = text.indexOf(find, position + 1)) > -1) {
			notepad_window.data('find_pos', position);
			notepad_set_cursor(notepad_window, position, find.length);
		} else {
			notepad_window.data('find_str', null);
			notepad_window.data('find_pos', null);
			orb_alert('No more appearances found.');
		}
	} else {
		notepad_find(notepad_window);
	}
}

function notepad_find_replace(notepad_window) {
	var replace_content =
		'<div class="notepad_find_replace">' +
		'<div class="form-group">' +
		'<input type="text" placeholder="Find" class="form-control find" />' +
		'</div>' +
		'<div class="form-group">' +
		'<input type="text" placeholder="Replace" class="form-control replace" />' +
		'</div>' +
		'<div class="btn-group">' +
		'<input type="button" value="Find & Replace" class="btn btn-default action" />' +
		'<input type="button" value="Cancel" class="btn btn-default cancel" />' +
		'</div>' +
		'</div>';

	var notepad_replace_window = $(replace_content).orb_window({
		header: 'Find & replace',
		width: 500,
		height: 135,
		icon: '/apps/notepad/notepad.png',
		dialog: true
	});

	notepad_replace_window.find('input.action').click(function() {
		var find = notepad_replace_window.find('input.find').val();
		var replace = notepad_replace_window.find('input.replace').val();

		if (find.length > 0) {
			var text = notepad_window.find('textarea').val();

			var position = 0;
			while ((position = text.indexOf(find, position)) > -1) {
				text = text.substr(0, position) + replace + text.substr(position + find.length);
				position += replace.length;
				notepad_window.data('changed', true);
			}

			notepad_window.find('textarea').val(text);
		}

		notepad_replace_window.close();
	});

	notepad_replace_window.find('input.cancel').click(function() {
		notepad_replace_window.close();
	});

	notepad_replace_window.open();
	notepad_replace_window.find('input.find').focus();
}

function notepad_menu_click(notepad_window, item) {
	notepad_window = notepad_window.find('div.notepad');

	switch (item) {
		case 'New':
			if (notepad_dont_discard(notepad_window)) {
				break;
			}

			notepad_window.find('textarea').val('');
			notepad_window.data('changed', false);
			notepad_window.data('filename', null);
			notepad_focus(notepad_window);
			break;
		case 'Open':
			if (notepad_dont_discard(notepad_window)) {
				break;
			}

			orb_file_dialog('Open', function(filename) {
				if (notepad_valid_extension(filename) == false) {
					orb_alert('Invalid file type.');
				} else {
					orb_file_open(filename, function(content) {
						notepad_window.find('textarea').val(content);
						notepad_window.data('changed', false);
						notepad_window.data('filename', filename);
						notepad_focus(notepad_window);
					}, function() {
						orb_alert('File not found.', 'Error');
					});
				}
			}, 'Documents');
			break;
		case 'Save':
			var filename = notepad_window.data('filename');
			if (filename != undefined) {
				notepad_save_file(notepad_window, filename);
				break;
			}
		case 'Save as':
			var filename = notepad_window.data('filename');
			if (filename == undefined) {
				var start_dir = 'Documents';
				var start_file = undefined;
			} else {
				var start_dir = orb_file_dirname(filename);
				var start_file = orb_file_filename(filename);
			}

			orb_file_dialog('Save', function(filename) {
				if (notepad_valid_extension(filename) == false) {
					filename += '.txt';
				}

				if (filename != notepad_window.data('filename')) {
					orb_file_exists(filename, function(exists) {
						if (exists) {
							if (confirm('File already exists. Overwrite?') == false) {
								return;
							}
						}
						notepad_save_file(notepad_window, filename);
					}, function() {
						orb_alert('Error while saving file.', 'Error');
					});
				} else {
					notepad_save_file(notepad_window, filename);
				}
			}, start_dir, start_file);
			break;
		case 'Find':
			notepad_find(notepad_window);
			break;
		case 'Find next':
			notepad_find_next(notepad_window);
			break;
		case 'Find & Replace':	
			notepad_find_replace(notepad_window);
			break;
		case 'Word wrap':
			if (notepad_window.find('textarea').attr('wrap') == 'off') {
				notepad_window.find('textarea').attr('wrap', '');
				orb_setting_set('applications/notepad/wordwrap', false);
			} else {
				notepad_window.find('textarea').attr('wrap', 'off');
				orb_setting_set('applications/notepad/wordwrap', true);
			}
			break;
		case 'Information':
			var content = notepad_window.find('textarea').val();

			var characters = content.length;

			content = content.trim().replace(/\s+/g, ' ');
			var words = content.match(/ /g);

			if (words != null) {
				words = words.length + 1;
			} else if (content.length > 0) {
				words = 1;
			} else {
				words = 0;
			}

			orb_alert('Characters: ' + characters + '\nWords: ' + words, 'Information');
			break;
		case 'Exit':
			notepad_window.close();
			break;
		case 'About':
			orb_alert('Notepad\nCopyright (c) by Hugo Leisink', 'About');
			break;
	}
}

function notepad_open_icon(icon) {
	var filename = orb_icon_to_filename(icon);

	if (filename == undefined) {
		return;
	}

	notepad_open(filename);
}

function notepad_open(filename = undefined) {
	var window_content =
		'<div class="notepad">' +
		'<textarea style="resize:none" class="form-control"></textarea>' +
		'</div>';

	var notepad_window = $(window_content).orb_window({
		header: 'Notepad',
		width: 800,
		height: 500,
		icon: '/apps/notepad/notepad.png',
		menu: {
			'File': [ 'New', 'Open', 'Save', 'Save as', '-', 'Exit' ],
			'Search': [ 'Find', 'Find next', 'Find & Replace' ],
			'View': [ 'Word wrap', 'Information' ],
			'Help': [ 'About' ]
		},
		menuCallback: notepad_menu_click,
		close: function() {
			if (notepad_dont_discard(notepad_window)) {
				return false;
			}
		}
	});

	notepad_window.data('changed', false);
	notepad_window.data('filename', null);
	notepad_window.find('textarea').on('input', function() {
		notepad_window.data('changed', true);
	});

	orb_setting_get('applications/notepad/wordwrap', function(value) {
		if (value == 'true') {
			notepad_window.find('textarea').attr('wrap', 'off');
		}
	});

	notepad_window.find('textarea').keydown(function(event) {
		notepad_tab_pressed(notepad_window, event);
	});

	if (filename != undefined) {
		if (notepad_valid_extension(filename) == false) {
			notepad_window.close();
			orb_alert('Invalid file type.');
		} else {
			orb_file_open(filename, function(content) {
				notepad_window.find('textarea').val(content);
				notepad_window.data('filename', filename);
				notepad_window.open();
				notepad_focus(notepad_window);
			}, function() {
				notepad_window.close();
				orb_alert('File not found.');
			});
		}
	} else {
		notepad_window.open();
		notepad_focus(notepad_window);
	}
}

$(document).ready(function() {
	var icon = '/apps/notepad/notepad.png';
	orb_startmenu_add('Notepad', icon, notepad_open);
	orb_upon_file_open('txt', notepad_open, icon);
	orb_upon_file_open('log', notepad_open, icon);
	orb_upon_file_open(ORB_NO_EXTENSION, notepad_open, icon);

	orb_contextmenu_extra_item('ots', 'Edit with Notepad', 'i-cursor', notepad_open_icon);
});
