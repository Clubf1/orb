<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Orb web desktop
	 * https://gitlab.com/hsleisink/orb
	 *
	 * Licensed under the GPLv2 License
	 */

	ob_start();

	session_name("Orb");
	$options = array(
		"expires"  => 0,
		"path"     => "/",
		"domain"   => "",
		"secure"   => true,
		"httponly" => true,
		"samesite" => "none");
	session_set_cookie_params($options);
	session_start();

	require "../libraries/error.php";
	require "../libraries/general.php";
	require "../libraries/orb.php";
	require "../libraries/user_website.php";

	$view = new view();

	$view->open_tag("output", array(
		"version" => ORB_VERSION,
		"debug"   => show_boolean(DEBUG_MODE)));

	$login = new login($view);
	$user_website = new user_website($view);

	if ($user_website->requested()) {
		/* Show user website
		 */
		$xslt_file = $user_website->show();
	} else if ($login->valid() == false) {
		/* Authentication
		 */
		$xslt_file = $login->execute();
	} else {
		/* Desktop
		 */
		$desktop = new desktop($view, $login->username);

		$xslt_file = $desktop->execute();
	}

	if (($errors = ob_get_contents()) != "") {
		if (is_true(DEBUG_MODE)) {
			$view->add_error($errors);
		} else {
			orb_log_error($errors);
		}
	}

	$view->close_tag();

	ob_end_clean();

	/* Generate output
	 */
	if (($html = $view->generate($xslt_file)) === false) {
		orb_exception_handler("XSLT error.");
	} else {
		print $html;
	}
?>
