<?xml version="1.0" ?>
<!--
	Copyright (c) by Hugo Leisink <hugo@leisink.net>
	This file is part of the Orb web desktop
	https://gitlab.com/hsleisink/orb

	Licensed under the GPLv2 License
-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" doctype-system="about:legacy-compat" />

<xsl:template match="/output">
<html lang="en">

<head>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta name="generator" content="File" />
<link rel="apple-touch-icon" href="/images/orb.png" />
<link rel="icon" href="/images/orb.png" />
<link rel="shortcut icon" href="/images/orb.png" />
<title>Orb web desktop</title>
<xsl:for-each select="styles/style">
<link rel="stylesheet" type="text/css" href="{.}" />
</xsl:for-each>
<xsl:for-each select="javascripts/javascript">
<script type="text/javascript" src="{.}" /><xsl:text>
</xsl:text></xsl:for-each>
</head>

<body>
<div class="content">
<h1>Orb error</h1>
<p><xsl:value-of select="error" /></p>
</div>
</body>

</html>
</xsl:template>

</xsl:stylesheet>
