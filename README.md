Orb
===

Orb is a free and open source web desktop, which simulates a Windows-like desktop in a web browser. You can use it to access files on a server or a NAS in an easy and secure way. It uses jQuery, Bootstrap and a PHP backend.
